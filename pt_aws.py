import boto3


aws_access_key = 'AKIAXVXQXU2G3DEGSHCV'
aws_secret_key = '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'
region_name = 'us-east-1' 




# Information on ec2 instance

instance_id = 'i-0f3ff6ce4692ff406'

ec2 = boto3.client('ec2', aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key, region_name=region_name)




response = ec2.describe_instances(InstanceIds=[instance_id])


instance = response['Reservations'][0]['Instances'][0]
print("EC2 Instance Information:")
print("Instance ID:", instance['InstanceId'])
print("Instance Type:", instance['InstanceType'])
print("Private IP Address:", instance['PrivateIpAddress'])
print("Public IP Address:", instance.get('PublicIpAddress', 'N/A'))
print("State:", instance['State']['Name'])
print("Tags:", instance.get('Tags', 'N/A'))

# Elastic ip information
if 'AssociationId' in instance:
    association_id = instance['AssociationId']
    response = ec2.describe_addresses(Filters=[{'Name': 'association-id', 'Values': [association_id]}])
    elastic_ip = response['Addresses'][0]
    print("\nElastic IP Information:")
    print("Allocation ID:", elastic_ip['AllocationId'])
    
else:
    print("\nNo Elastic IP associated with the instance.")


# EBS volume information
print("\nEBS Volumes Information:")
for volume in instance['BlockDeviceMappings']:
    volume_id = volume['Ebs']['VolumeId']
    volume_info = ec2.describe_volumes(VolumeIds=[volume_id])['Volumes'][0]
    print("Volume ID:", volume_info['VolumeId'])
    print("Volume Type:", volume_info['VolumeType'])
    print("Size (GiB):", volume_info['Size'])
    print("State:", volume_info['State'])
    print("Attach Time:", volume_info.get('AttachTime', 'N/A'))
    print("-----------------------------")
